#ifndef __SPI_H
#define __SPI_H	
#include "sys.h"


#define LCD_CS_Clr()   GPIO_ResetBits(GPIOA,GPIO_Pin_0)//CS
#define LCD_CS_Set()   GPIO_SetBits(GPIOA,GPIO_Pin_0)

#define LCD_DC_Clr()   GPIO_ResetBits(GPIOB,GPIO_Pin_8)//DC
#define LCD_DC_Set()   GPIO_SetBits(GPIOB,GPIO_Pin_8)

#define LCD_BLK_Clr()  GPIO_ResetBits(GPIOB,GPIO_Pin_7)//BLK
#define LCD_BLK_Set()  GPIO_SetBits(GPIOB,GPIO_Pin_7)

#define LCD_RES_Clr()  GPIO_ResetBits(GPIOB,GPIO_Pin_9)//RES
#define LCD_RES_Set()  GPIO_SetBits(GPIOB,GPIO_Pin_9)

void LCD_GPIO_Init(void);
void SPI1_Init(void);

void GPIO_Other_Init(void);
void SPI1_DMA_Tx_Init(DMA_Channel_TypeDef* DMA_CHx ,u32 memadr , u16 bufsize);


void SPI1_Send_Data_Word(u16 Data);
void SPI1_Send_Data_Byte(u8 Data);
void SPI_Size_Config_16Bit(void);
void SPI_Size_Config_8Bit(void);
void SPI1_DMA_SendByte(u8 *Tx_DATA , u16 SIZE);
void SPI1_DMA_SendWord(u16 *Tx_DATA , u16 SIZE);
#endif
