#include "sys.h"

static rt_thread_t key_thread = RT_NULL;
static void key_thread_entry(void *parameter);

static rt_thread_t lcd_thread = RT_NULL;
static void lcd_thread_entry(void *parameter);

rt_uint8_t key_value=0;
extern const unsigned char gImage_1[3200];

 int main(void)
 {		
	 	LCD_Init();//LCD初始化
		LCD_Fill(0,0,LCD_W,LCD_H,WHITE);
	 
	 	key_thread = rt_thread_create("key_thread", key_thread_entry, RT_NULL, 256, 2, 60);
		if(key_thread != RT_NULL)
		{
			rt_thread_startup(key_thread);
			rt_kprintf("key_thread_startup_Success\n");
		}
		
		lcd_thread = rt_thread_create("lcd_thread", lcd_thread_entry, RT_NULL, 256, 3, 60);
		if(lcd_thread != RT_NULL)
		{
			rt_thread_startup(lcd_thread);
			rt_kprintf("lcd_thread_startup_Success\n");
		}
 }

 
void key_thread_entry(void *parameter)
{
	while (1)
	{	
		
		key_value = KEY_Scan(0);
		rt_kprintf("key_value:%d\r\n",key_value);
		rt_thread_delay(60);
	}	
}

void lcd_thread_entry(void *parameter)
{
	rt_uint8_t i=0;
	while (1)
	{	
		switch(key_value)
		{				 
		
			case KEY2_PRES:	//控制LED0翻转
				GPIO_SetBits(GPIOA,GPIO_Pin_4);
				GPIO_SetBits(GPIOA,GPIO_Pin_6);
				LCD_Fill(0,0,LCD_W,LCD_H,GRAY);
				LCD_ShowChinese(0,0,"硬禾小马",RED,GRAY,32,0);
			break;
			
			case KEY1_PRES:	//控制LED1翻转		
				GPIO_ResetBits(GPIOA,GPIO_Pin_4);
				GPIO_ResetBits(GPIOA,GPIO_Pin_6);
				LCD_Fill(0,0,LCD_W,LCD_H,RED);					
				LCD_ShowString(0,40,"LCD_W:",RED,WHITE,16,0);
				LCD_ShowIntNum(48,40,LCD_W,3,RED,WHITE,16);
				LCD_ShowString(0,80,"LCD_H:",RED,WHITE,16,0);
				LCD_ShowIntNum(48,80,LCD_H,3,RED,WHITE,16);
			break;
			case KEY3_PRES:	//控制LED1翻转		
			LCD_Fill(0,0,LCD_W,LCD_H,WHITE);
			for(i=0;i<3;i++)
			{
				LCD_ShowPicture(40*i,120,40,40,gImage_1);
			}
			break;
		}
		rt_thread_delay(50);
	}	
}

