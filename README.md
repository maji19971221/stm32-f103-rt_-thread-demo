# STM32F103-RT_Thread Demo

#### 介绍
两个工程，都是STM32F103跑RT Thread。一个是按键控制LCD屏的显示。一个是CH395以太网接收芯片的控制（跑的TCP-Client）。
第一个工程是基于硬禾学堂STM32F103的板子写的。
第二个工程使用的是正点原子战舰开发板+沁恒微电子（WCH）CH395模块。



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
