/********************************** (C) COPYRIGHT  *******************************
* File Name          : CH395_network_operation.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2020/10/12
* Description        : CH395基本操作步骤
*******************************************************************************/ 
#include "CH395_network_operation.h"
#include "CH395_interface_operation.h"
#include "MCU_communication_interface.h"
#include "CH395inc.h"
#include "CH395.h"
#include "usart.h"
#include "string.h"
#include "stdio.h"
#include "sys.h"
#include "rthw.h"
/* CH395本地配置 */
struct  _CH395_SYS  ch395inf;      /* 保存CH395信息 */
extern uint8_t local_ip[];        /* CH395IP地址 */
extern uint8_t gateway_ip[] ;     /* CH395网关 */
extern uint8_t mask_ip[];         /* CH395子网掩码 */

/* CH395socket配置 */
extern uint16_t source_port[8];
extern struct _SOCK_INF  sockinf[4];



void rt_hw_us_delay(uint32_t us)
{
	rt_uint32_t start,now,delta,reload,us_tick;
	start = SysTick -> VAL;
	reload = SysTick ->LOAD;
	us_tick = SystemCoreClock / 1000000UL;
	do{
		now = SysTick ->VAL;
		delta = start>now?start - now: reload + start-now;
	}while(delta<us_tick*us);
}
/*******************************************************************************
* Function Name  : CH395_if_init
* Description    : CH32的硬件接口初始化
* Input          : None
* Return         : None
*******************************************************************************/
void CH395_if_init(void)
{
	
#ifdef USE_USART2_CTRL_CH395
	USART2_init();
#else 
	SPI_FullDuplex_Init();/* SPI初始化和SPI相关的GPIO口初始化 */
#endif
	control_io_Init();    /* io口相关的初始化 */
}

/*******************************************************************************
* Function Name  : CH395_init
* Description    : CH395初始化
* Input          : None
* Return         : 初始化状态
*******************************************************************************/
uint8_t CH395_init(void)
{
    uint8_t i;
//	uint8_t cnt=255;
	
	
	i=ch395_cmd_check_exist(0x65);
	if(i != 0x9a)return CH395_ERR_UNKNOW;
	i = ch395_cmd_get_ver();
    rt_kprintf("CH395VER : %2x\r\n",i);
#ifdef USE_USART2_CTRL_CH395	
#ifdef UART_WORK_BAUDRATE
    ch395_cmd_set_uart_baud_rate(UART_WORK_BAUDRATE);  /* 设置波特率 */   
    rt_thread_delay(5);
    set_usart2_bandrate(UART_WORK_BAUDRATE);
#endif
#endif
    ch395_cmd_reset();                            /* 软复位命令 */
    rt_thread_delay(300);                                /* 软复位之后起码延时50毫秒 */
    ch395_cmd_set_ipaddr(ch395inf.IPAddr);        /* 设置CH395的IP地址 */
    ch395_cmd_set_gw_ipaddr(ch395inf.GWIPAddr);   /* 设置网关地址 */
    ch395_cmd_set_maskaddr(ch395inf.MASKAddr);    /* 设置子网掩码，默认为255.255.255.0*/
		ch395_set_start_para(FUN_PARA_FLAG_TCP_SERVER|SOCK_DISABLE_SEND_OK_INT);/*TCP SERVER支持多连接时，需初始化此启动参数*/
		rt_thread_delay(10); 
    i = ch395_cmd_init();                         /* 初始化CH395芯片 */
    return i;
}

/*******************************************************************************
* Function Name  : ch395_inf_init
* Description    : CH395本地信息初始化
* Input          : None
* Return         : 初始化状态
*******************************************************************************/
void ch395_inf_init(void)
{
    memset(&ch395inf, 0, sizeof(ch395inf));        /* 将CH395Inf全部清零 */
    memcpy(ch395inf.IPAddr, local_ip, 4);          /* 将IP地址写入CH395Inf中 */
    memcpy(ch395inf.GWIPAddr, gateway_ip, 4);      /* 将网关IP地址写入CH395Inf中 */
    memcpy(ch395inf.MASKAddr, mask_ip, 4);         /* 将子网掩码写入CH395Inf中 */
}

/*******************************************************************************
* Function Name  : ch395_socket_init
* Description    : CH395socke信息初始化
* Input          : None
* Return         : None
*******************************************************************************/
void ch395_socket_init(void)
{
	uint8_t i;
	
	for(i=0;i<8;i++)
	{
		memset(&sockinf[i],i,sizeof(sockinf[i]));                      /* 将SockInf[0]全部清零*/
		memcpy(&sockinf[i].SourPort, &source_port[i], 2);              /* 将本地端口写入 */
		sockinf[i].ProtoType = PROTO_TYPE_TCP;                         /* TCP模式 */
	}
	sockinf[0].TcpMode = TCP_SERVER_MODE;                              /* TCP客户端模式 */ 
}

/*******************************************************************************
* Function Name  : socket_open
* Description    : CH395socke开启
* Input          : None
* Return         : None
*******************************************************************************/
void socket_open(void)
{
	uint8_t rc,i;
	
	ch395_set_socket_prot_type(0,sockinf[0].ProtoType);
	ch395_set_socket_sourport(0,sockinf[0].SourPort);
	rc=ch395_open_socket(0);
	stop_if_error(rc); 
	rc=ch395_tcp_listen(0);
    stop_if_error(rc); 
	for(i=1;i<8;i++)
	{
		ch395_set_socket_prot_type(i,sockinf[i].ProtoType);
	    ch395_set_socket_sourport(i,server_port);
	}
}

/*********************************************************************************
* Function Name  : Socket_R_S_Buf_Modify
* Description    : Socket收发缓冲区定义
* Input          : None
* Output         : None
* Return         : None
**********************************************************************************/
void Socket_R_S_Buf_Modify(void )
{

   ch395_set_socket_recv_buf(0,0,4);                                     /* Socket 0 ，接收缓冲区4*512 = 2K，发送缓冲区2*512 = 1K*/
   ch395_set_socket_send_buf(0,4,2);  
  
   ch395_set_socket_recv_buf(1,6,4);                                     /* Socket 1 */
   ch395_set_socket_send_buf(1,10,2);  
  
   ch395_set_socket_recv_buf(2,12,4);                                    /* Socket 2 */
   ch395_set_socket_send_buf(2,16,2);  
  
   ch395_set_socket_recv_buf(3,18,4);                                    /* Socket 3 */
   ch395_set_socket_send_buf(3,22,2);  
  
   ch395_set_socket_recv_buf(4,24,4);                                    /* Socket 4 */
   ch395_set_socket_send_buf(4,28,2);  
  
   ch395_set_socket_recv_buf(5,30,4);                                    /* Socket 5 */
   ch395_set_socket_send_buf(5,34,2);
  
   ch395_set_socket_recv_buf(6,36,4);                                    /* Socket 6 */
   ch395_set_socket_send_buf(6,40,2);  
  
   ch395_set_socket_recv_buf(7,42,4);                                    /* Socket 7 */
   ch395_set_socket_send_buf(7,46,2);  

}
