/********************************** (C) COPYRIGHT  *******************************
* File Name          : MCU_communication_interface.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2020/10/12
* Description        : CH32 沟通接口配置
*******************************************************************************/
#include "stdio.h"
#include "MCU_communication_interface.h"
#include "usart.h"
#include "ch395.h"
#include "sys.h"
void control_io_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;


	/* CH395中断输入脚 PA0*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;	/* CH395中断配置：单片机引脚设为上拉输出 */
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   /* */
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	
	/* 控制CH395硬件复位脚 PA1*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;	/* 硬件复位输出配置为开漏输出 */
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   /* */
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	GPIO_SetBits(GPIOA,GPIO_Pin_1);
	

}

/*
------------------------------------SPI接口相关---------------------------------
*/

/*******************************************************************************
* Function Name  : SPI_FullDuplex_Init
* Description    : Configuring the SPI for full-duplex communication.
* Input          : None
* Return         : None
*******************************************************************************/ 
#ifdef USE_SPI1_CTRL_CH395
void SPI_FullDuplex_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;
	
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_SPI1, ENABLE );	
		
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );	

	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	
	GPIO_Init( GPIOA, &GPIO_InitStructure );	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;		
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init( GPIOA, &GPIO_InitStructure );
	
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;	

	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;	
	
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;	
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;	
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;	
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;	
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;	
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init( SPI1, &SPI_InitStructure );
	
	SPI_Cmd( SPI1, ENABLE );
}

#else 
void USART2_init(void)
{
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	

	/* USART2 TX-->A.2   RX-->A.3 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	

	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

	USART_Init(USART2, &USART_InitStructure); 
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	USART_Cmd(USART2, ENABLE); 
}

void set_usart2_bandrate(uint32_t bandrate)
{
	USART_InitTypeDef USART_InitStructure;
	
	USART_InitStructure.USART_BaudRate = bandrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

	USART_Init(USART2, &USART_InitStructure); 
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	USART_Cmd(USART2, ENABLE);
}
#endif

/**
*描述：语句基本构成：写命令
 */
void ch395_write_cmd( uint8_t mCmd )
{
//  ch395_scs_hign;                /* 防止CS原来为低，先将CS置高 */
    ch395_scs_low;                 /* 命令开始，CS拉低 */
#ifdef USE_USART2_CTRL_CH395
    ch395_write_data(0x57);
//	rt_thread_delay(2);
	ch395_write_data(0xab);
	
#endif
//	rt_thread_delay(2);
    ch395_write_data(mCmd);        /* SPI发送命令码 */
		rt_hw_us_delay(2);                   /* 必要延时,延时1.5uS确保读写周期不小于1.5uS */

}

uint8_t SPI1_ReadWriteByte(uint8_t TxData)
{		
	u8 i=0;		
	
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET) 
	{
		i++;
		if(i>200)return 0;
	}	
	
	SPI_I2S_SendData(SPI1, TxData); 
	i=0;

	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET)
	{
		i++;
		if(i>200)return 0;
	}	  
	return SPI_I2S_ReceiveData(SPI1); 				    
}

/**
----------------------------------------  CH395硬件写取函数 -----------------------------------------
---------------------------------------  移植更换API入口位置 -----------------------------------------
 */
void ch395_write_data( uint8_t mData )
{	
#ifdef USE_SPI1_CTRL_CH395
	SPI1_ReadWriteByte(mData);
#else
	while(USART_GetFlagStatus(USART2,USART_FLAG_TC)==RESET);
	USART_SendData(USART2, mData); 
	
	printf("mData=%02x\r\n",mData);
#endif
}

/**
-----------------------------------------  CH395硬件读取函数 -----------------------------------------
----------------------------------------  移植更换API入口位置 -----------------------------------------
 */
uint8_t ch395_read_data( void )
{
	uint8_t o;
#ifdef USE_SPI1_CTRL_CH395
	return  SPI1_ReadWriteByte(0xff);
#else
	while(USART_GetFlagStatus(USART2,USART_IT_RXNE) == RESET)
	{
		
	}
	if(USART_GetFlagStatus(USART2,USART_IT_RXNE) != RESET)
	{
		o = (uint8_t)USART_ReceiveData(USART2); 
	}
	return o;
#endif
}

/*
-------------------------------------------------------------------------------------------------------

                                        USART接口相关

-------------------------------------------------------------------------------------------------------
*/


