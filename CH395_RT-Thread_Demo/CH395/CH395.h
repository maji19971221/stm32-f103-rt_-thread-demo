#ifndef  _CH395_H__
#define  _CH395_H__

#define TCP_CLIENT_MODE                                      0
#define TCP_SERVER_MODE                                      1


struct _CH395_SYS
{
    uint8_t   IPAddr[4];           /* CH395IP地址 32bit*/
    uint8_t   GWIPAddr[4];         /* CH395网关地址 32bit*/
    uint8_t   MASKAddr[4];         /* CH395子网掩码 32bit*/
    uint8_t   MacAddr[6];          /* CH395MAC地址 48bit*/
    uint8_t   PHYStat;             /* CH395 PHY状态码 8bit*/
    uint8_t   MackFilt;            /* CH395 MAC过滤，默认为接收广播，接收本机MAC 8bit*/
    uint32_t  RetranCount;         /* 重试次数 默认为10次*/
    uint32_t  RetranPeriod;        /* 重试周期,单位MS,默认200MS */
    uint8_t   IntfMode;            /* 接口模式 */
    uint8_t   UnreachIPAddr[4];    /* 不可到达IP */
    uint16_t  UnreachPort;         /* 不可到达端口 */
};

struct _SOCK_INF
{
    uint8_t  IPAddr[4];            /* socket目标IP地址 32bit*/
    uint8_t  MacAddr[6];           /* socket目标地址 48bit*/
    uint8_t  ProtoType;            /* 协议类型 */
    uint8_t  ScokStatus;           /* socket状态，参考scoket状态定义 */
    uint8_t  TcpMode;              /* TCP模式 */
    uint32_t IPRAWProtoType;       /* 协议类型 */
    uint16_t DesPort;              /* 目的端口 */
    uint16_t SourPort;             /* 目的端口 */
    uint16_t SendLen;              /* 发送数据长度 */
    uint16_t RemLen;               /* 剩余长度 */
    uint8_t  *pSend;               /* 发送指针 */
};
#define default_tcp_mss   512       /* 默认的MSS大小设置 */
#define tcp_mss_dma       1460



#define DEF_KEEP_LIVE_IDLE                           (20*1000)        /* 空闲时间 */
#define DEF_KEEP_LIVE_PERIOD                         (15*1000)        /* 间隔15s，发送一次KEPPLIVE数据包 */                  
#define DEF_KEEP_LIVE_CNT                            200                /* 重试次数  */

/* CH32操作CH395的硬件SPI接收开启DMA选择 0关闭 1开启 只用在SPI_DMA工程*/
#define SPI1_DMA_EN   0
/* CH32操作CH395的硬件接口选择 */

#if 1
#define USE_SPI1_CTRL_CH395         /* 使用SPI口作为默认的控制H395的接口 */
#else
#define USE_USART2_CTRL_CH395       /* 使用USART2口作为默认的控制CH395的接口 */     
#endif

#define UART_WORK_BAUDRATE  9600

#define stop_if_error(x)\
if(x!=0){printf("Error: %02x,@ line %d of \"%s\".\r\n", (uint16_t)x,__LINE__,__FILE__);while(1);}

#define server_port source_port[0]                 /* 服务器端口 */

#endif
