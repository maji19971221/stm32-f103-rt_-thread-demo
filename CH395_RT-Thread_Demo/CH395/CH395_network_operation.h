#ifndef __CH395_network_operation__
#define __CH395_network_operation__

#ifndef uint8_t
typedef unsigned char uint8_t;
#endif

#ifndef uint16_t
typedef unsigned short uint16_t;
#endif

#ifndef uint32_t
typedef unsigned int uint32_t;
#endif

void CH395_if_init(void);
uint8_t CH395_init(void);
void ch395_inf_init(void);
uint8_t ch395_cmd_get_phy_status(void);
void ch395_set_socket_recv_buf(uint8_t sockindex, uint8_t startblk, uint8_t blknum);
void ch395_set_socket_send_buf(uint8_t sockindex, uint8_t startblk, uint8_t blknum);
void ch395_set_socket_prot_type(uint8_t sockindex, uint8_t prottype);
void socket_open(void);
void ch395_socket_init(void);
void Socket_R_S_Buf_Modify(void );
#endif
