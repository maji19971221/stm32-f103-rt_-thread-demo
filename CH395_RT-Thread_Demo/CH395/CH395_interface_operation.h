#ifndef _CH395_interface_operation_h__
#define _CH395_interface_operation_h__

#ifndef uint8_t
typedef unsigned char uint8_t;
#endif

#ifndef uint16_t
typedef unsigned short uint16_t;
#endif

#ifndef uint32_t
typedef unsigned int uint32_t;
#endif
void ch395_cmd_hard_reset(void);
void rt_hw_us_delay(uint32_t us);
void ch395_cmd_reset(void);
uint8_t ch395_cmd_get_ver(void);
void ch395_write_cmd( uint8_t mCmd );
uint8_t ch395_cmd_check_exist(uint8_t testdata);
void ch395_cmd_set_ipaddr(uint8_t *ipaddr);
void ch395_cmd_set_gw_ipaddr(uint8_t *gwipaddr);
void ch395_cmd_set_maskaddr(uint8_t *maskaddr);
uint8_t ch395_get_cmd_status(void);
uint8_t ch395_cmd_init(void);
void ch395_set_socket_sourport(uint8_t sockindex, uint16_t surprot);
uint8_t ch395_open_socket(uint8_t sockindex);
uint8_t  ch395_close_socket(uint8_t sockindex);
uint8_t ch395_tcp_listen(uint8_t sockindex);
uint8_t ch395_get_socket_int(uint8_t sockindex);
uint16_t ch395_get_recv_length(uint8_t sockindex);
void ch395_get_recv_data(uint8_t sockindex, uint16_t len, uint8_t *pbuf);
uint16_t ch395_cmd_get_glob_int_status_all(void);
void ch395_cmd_get_unreachippt(uint8_t *list);
void ch395_cmd_get_remoteipp(uint8_t sockindex, uint8_t *list);
void ch395_set_start_para(uint32_t mdata);
void ch395_set_tcpmss(uint16_t tcpmss);
void ch395_send_data(uint8_t sockindex, uint8_t *databuf, uint16_t len);
void ch395_cmd_set_uart_baud_rate(uint32_t baudrate);

#endif
