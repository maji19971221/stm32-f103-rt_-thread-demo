#ifndef _mcu_com_if__
#define _mcu_com_if__

#include "sys.h" 

#ifndef uint8_t
typedef unsigned char uint8_t;
#endif

#ifndef uint16_t
typedef unsigned short uint16_t;
#endif

#ifndef uint32_t
typedef unsigned int uint32_t;
#endif

//#define ch395_scs_hign
//#define ch395_scs_low  

#ifdef USE_SPI1_CTRL_CH395
#define ch395_scs_hign (*(uint32_t*)(GPIOA_BASE+0x10))|=(1<<4)
#define ch395_scs_low  (*(uint32_t*)(GPIOA_BASE+0x14))|=(1<<4)
#else
#define ch395_scs_hign 
#define ch395_scs_low  
#endif

#define ch32_gpio_in_a (*(uint32_t*)(GPIOA_BASE+0x08))
#define ch395_exti_stat (ch32_gpio_in_a&0x0001)


void SPI_FullDuplex_Init(void);
void control_io_Init(void);
void ch395_write_cmd( uint8_t mCmd );
void ch395_write_data( uint8_t mData );
uint8_t ch395_read_data( void );
void USART2_init(void);
void set_usart2_bandrate(uint32_t bandrate);
#endif
