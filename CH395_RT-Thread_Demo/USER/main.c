#include "sys.h"

#include "CH395_interface_operation.h"        /* 对外提供函数接口 */

/* 本地IP设置 */
const uint8_t local_ip[4] = {192,168,1,20};                         /* CH395IP地址 */
const uint8_t gateway_ip[4] = {192,168,1,1};                        /* CH395网关 */
const uint8_t mask_ip[4] = {255,255,255,0};                        /* CH395子网掩码 */
/* socket 相关定义*/
const uint8_t  Socket0DesIP[4] = {192,168,1,30};                      /* Socket 0目的IP地址 */
const uint16_t Socket0DesPort = 2000;                                  /* Socket 0目的端口 */
const uint16_t Socket0SourPort = 5000;                                 /* Socket 0源端口 */
uint16_t source_port[8]={100};              /* 各个socket 的b本地IP */
/* 常用变量定义 */
uint8_t  socket_buff[8][512];                                           /* 数据缓冲区 */
struct _SOCK_INF sockinf[8];                                      /* 保存Socket信息 */
struct _CH395_SYS  CH395Inf;                                    /* 保存CH395信息 */





static rt_thread_t led_thread = RT_NULL;
static void led_thread_entry(void *parameter);

static rt_thread_t key_thread = RT_NULL;
static void key_thread_entry(void *parameter);

static rt_thread_t CH395_thread = RT_NULL;
static void CH395_thread_entry(void *parameter);
//static rt_sem_t key_sem = RT_NULL;

u8 key;


void CH395SocketInterrupt(uint8_t sockindex)
{
   uint8_t  sock_int_socket,i;
   uint16_t len;
   uint16_t tmp;

   sock_int_socket = ch395_get_socket_int(sockindex);                /* 获取socket 的中断状态 */
   if(sock_int_socket & SINT_STAT_SENBUF_FREE)                       /* 发送缓冲区空闲，可以继续写入要发送的数据 */
   {
	   
	#if !SPI1_DMA_EN
//	   delay_us(300);
	   
    #endif
   }
   if(sock_int_socket & SINT_STAT_SEND_OK)                           /* 发送完成中断 */
   {
//	   printf("send ok!\n");
   }
   if(sock_int_socket & SINT_STAT_RECV)                              /* 接收中断 */
   {
       len = ch395_get_recv_length(sockindex);                        /* 获取当前缓冲区内数据长度 */
	   
	   #if !SPI1_DMA_EN
//		printf("receive len = %d\n",len);
//	   delay_us(200);
	   #endif

#if SPI1_DMA_EN
	  if(len == 0)return;
      if(len > tcp_mss_dma)len = tcp_mss_dma; 
      ch395_get_recv_data(sockindex,len,RxData);     /* 读取数据 */
			ch395_send_data(sockindex,TxData,len);
#else	  
	  if(len == 0)return;
      if(len > default_tcp_mss)len = default_tcp_mss;                /* MyBuffer缓冲区长度为512*/
	  ch395_get_recv_data(sockindex,len,socket_buff[sockindex]);     /* 读取数据 */
      ch395_send_data(sockindex,socket_buff[sockindex],len);
#endif
   }
   if(sock_int_socket & SINT_STAT_CONNECT)                           /* 连接中断，仅在TCP模式下有效*/
   {
	   uint8_t target_inf[6];
	   
       if(sockinf[sockindex].TcpMode == TCP_SERVER_MODE)             /* 如果socket 为服务器模式，用户可以获取远端的IP和端口*/
       {
           ch395_cmd_get_remoteipp(sockindex,target_inf);
           tmp = (uint16_t)(target_inf[5]<<8) + target_inf[4];
           rt_kprintf("IP address = %d.%d.%d.%d\n",(uint16_t)target_inf[0],(uint16_t)target_inf[1],(uint16_t)target_inf[2],(uint16_t)target_inf[3]);    
           rt_kprintf("Port = %d\n",tmp);    
       }
   }
   /*
   **产生断开连接中断和超时中断时，CH395默认配置是内部主动关闭，用户不需要自己关闭该Socket，如果想配置成不主动关闭Socket需要配置
   **SOCK_CTRL_FLAG_SOCKET_CLOSE标志位（默认为0），如果该标志为1，CH395内部不对Socket进行关闭处理，用户在连接中断和超时中断时调用
   **CH395CloseSocket函数对Socket进行关闭，如果不关闭则该Socket一直为连接的状态（事实上已经断开），就不能再去连接了。
   */
   /*SOCK_CTRL_FLAG_SOCKET_CLOSE被置位后，下面的中断需要关闭Socket，再进行其他操作。不置位则不需要CH395CloseSocket，CH395内部已经关闭。*/
   if(sock_int_socket & SINT_STAT_DISCONNECT)                        /* 断开中断，仅在TCP模式下有效 */
   {
	   
	   i = ch395_close_socket(sockindex);                             
	   stop_if_error(i);
	   rt_thread_mdelay(2);
	   i = ch395_open_socket(sockindex);
	   stop_if_error(i);
   }
	if(sock_int_socket & SINT_STAT_TIM_OUT)                           /* 超时中断，仅在TCP模式下有效 */
	{
		rt_kprintf("SINT_STAT_TIM_OUT");
	}
}
void CH395GlobalInterrupt(void)
{
	uint16_t  init_status,tem_flag;
	uint8_t  i,buf[10]; 

	init_status = ch395_cmd_get_glob_int_status_all();
	if(init_status & GINT_STAT_UNREACH)                              /* 不可达中断，读取不可达信息 */
	{
		ch395_cmd_get_unreachippt(buf);                                
	}
	if(init_status & GINT_STAT_IP_CONFLI)                            /* 产生IP冲突中断，建议重新修改CH395的 IP，并初始化CH395*/
	{
	}
	if(init_status & GINT_STAT_PHY_CHANGE)                           /* 产生PHY改变中断*/
	{
		rt_kprintf("Init status : GINT_STAT_PHY_CHANGE\n");
	}
//	if(init_status & GINT_STAT_SOCK0)
//	{
//	    CH395SocketInterrupt(0);								     /* 处理socket 0中断*/
//	}
	tem_flag=GINT_STAT_SOCK0;
	for(i=0;i<8;i++)
	{
		if(init_status & tem_flag)
		   CH395SocketInterrupt(i);   
		tem_flag=(1<<(4+1+i));
	}
	rt_kprintf("Over\n");
}
int main(void)
{	
		rt_int8_t rc;

		ch395_inf_init();                            /* CH395本地配置信息初始化 */
		ch395_cmd_hard_reset();
		rt_thread_delay(50);
		rc=CH395_init();
		stop_if_error(rc);
		ch395_cmd_get_phy_status();
		rt_kprintf("CH395 Connect Ethernet Start\n");  
		while(ch395_cmd_get_phy_status()<0x02)       /* 等待物理层连接上 */
		{
			printf("Waiting for ethernet connection...\n");
			rt_thread_delay(200);
		}
		rt_kprintf("CH395 SocketInit Start\n");
		ch395_socket_init();                         /* socket信息初始化 */
		socket_open();                               /* 打开socket */
		
		led_thread = rt_thread_create("led_thread", led_thread_entry, RT_NULL, 256, 5, 30);
		if(led_thread != RT_NULL)
		{
			rt_kprintf("led_thread  Start\n");
			rt_thread_startup(led_thread);
		}
		key_thread = rt_thread_create("key_thread", key_thread_entry, RT_NULL, 256, 4, 30);
		if(key_thread != RT_NULL)
			rt_thread_startup(key_thread);
	
		
		CH395_thread = rt_thread_create("CH395_thread", CH395_thread_entry, RT_NULL, 256, 6, 100);
		if(CH395_thread != RT_NULL)
		{
			rt_thread_startup(CH395_thread);
			rt_kprintf("rt_thread_startup creat success!!!\r\n");
		}
}

void CH395_thread_entry(void *parameter)
{
	while(1)
	{
		if(ch395_exti_stat==0)
		{
				CH395GlobalInterrupt();
//				rt_kprintf("123\r\n");
//				rt_thread_suspend(CH395_thread);
		}
	}
}
void led_thread_entry(void *parameter)
{
	while (1)
	{	
//		rt_kprintf("key_led:%d\n",key);
		switch(key)
		{			
//				
				rt_kprintf("key_led1:%d\n",key);
				case KEY2_PRES:	//控制LED0翻转
					LED0=!LED0;
					key=0;
				break;
				case KEY1_PRES:	//控制LED1翻转	 
					LED1=!LED1;
					key=0;
				break;
				case KEY0_PRES:	//同时控制LED0,LED1翻转 
					LED0=!LED0;
					LED1=!LED1;
					key=0;
				break;
		}
//		rt_kprintf("led_thread_over\n");
		rt_thread_delay(20);
	}	
}

void key_thread_entry(void *parameter)
{
	while(1)
	{
//		rt_kprintf("key_thread_entry\n");
		key=KEY_Scan(1);	//得到键值
		rt_kprintf("key:%d\n",key);
		rt_thread_delay(20);
//		rt_kprintf("key_thread_over\n");
	}
}


